import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router);

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/char/:charId',
        component: () => import('./views/char.vue'),
        redirect: '/char/:charId/main',
        children: [
            {
                path: 'main',
                name: 'mainInfo',
                component: () => import('./views/character/main.vue')
            },
            {
                path: 'extraStats',
                name: 'extraStats',
                component: () => import('./views/character/extraStats.vue')
            }
        ]
    },
    {
        path: '/about',
        name: 'about',
        component: () => import('./views/About.vue'),
        meta: {
            title: 'О нас'
        }
    },
    {
        path: '/404',
        name: 'Not Found',
        component: () => import('./views/notFound.vue'),
        meta: {
            title: 'Страница не найдена'
        }
    },
    {
        path: '*',
        redirect: '/404'
    }
];

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

