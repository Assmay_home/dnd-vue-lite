import Vue from 'vue'
import App from './App.vue'
import router from './router'
import normalize from 'normalize.css'
import store from './store'

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');

Vue.use(normalize);

const siteName = 'DnD 5e Charsheet';

router.beforeEach((to, from, next) => {
  if (to.meta.title) {
    document.title = to.meta.title;
    next();
  } else {
    document.title = siteName;
    next();
  }
});
