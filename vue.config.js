module.exports = {
  outputDir: 'public_html',
  runtimeCompiler: true,
  css: {
    modules: true,
    sourceMap: true
  }
}
